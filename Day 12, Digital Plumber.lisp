;;;; --- Day 12: Digital Plumber ---

(defparameter *puzzle-input* "Day 12, Input.txt")

;;; Sample input line: "0 <-> 950, 1039"

(ql:quickload "cl-ppcre")

(defun read-input-file (&optional (filename *puzzle-input*))
  (with-open-file (in filename)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(defun process-input-line (line)
  (mapcar #'parse-integer
          (cl-ppcre:all-matches-as-strings "\\w+" line)))

(defun group-with-node (node connections)
  (let ((connected '()))
    (labels ((find-connected (n)
               (push n connected)
               (let* ((neighbors (cdr (assoc n connections)))
                      (unseen (set-difference neighbors connected)))
                 (mapc #'find-connected unseen))))
      (find-connected node))
    connected))

;;; Solution to Part 1: 306
(defun count-connected-to-0 ()
  (let ((connections (mapcar #'process-input-line (read-input-file))))
    (length (group-with-node 0 connections))))

;;; Solution to Part 2: 200

(defun groups (connections)
  (let ((node-list (mapcar #'first connections)))
    (labels ((group-from (nodes)
               (let* ((group (group-with-node (first nodes) connections))
                      (remaining (set-difference nodes group)))
                 (if remaining
                     (cons group (group-from remaining))
                     group))))
      (group-from node-list))))

(defun count-of-groups ()
  (let ((connections (mapcar #'process-input-line (read-input-file))))
    (length (groups connections))))
