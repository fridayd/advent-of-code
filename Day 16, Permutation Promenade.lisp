;;;; --- Day 16: Permutation Promenade ---

(declaim (optimize (speed 3) (safety 0) (debug 0)))

(deftype dance-line () '(simple-array character (16)))
(deftype line-number () '(integer 0 15))
(deftype line-char () 'standard-char)

(ql:quickload "cl-ppcre" :silent t)
(ql:quickload "cl-arrows" :silent t)
(use-package :cl-ppcre)
(use-package :cl-arrows)

(defparameter *puzzle-input* "Day 16, Input.txt")
(defparameter *sample-input* "x3/10,pl/g,s9,x6/15,s10,x9/1,pi/c")

(defun read-puzzle-input (&optional (filename *puzzle-input*))
  (with-open-file (in filename)
    (first
     (loop for line = (read-line in nil)
           while line
           collect line))))

(defun pre-process-input (input)
  "Changes input like this: 'x3/10,pl/g,s9'
To output like this: ((x 3 10) (p #\l #\g) (s 9))"
  (-<> input
       (regex-replace-all ",?([spx])(\\w+)(/\\w+)?,?" <> "(\\1 \\2\\3) ")
       (regex-replace-all "([a-z])/([a-z])" <> "#\\\\\\1 #\\\\\\2")
       (regex-replace-all "/" <> " ")
       (regex-replace "(.*) " <> "(\\1)")))

(defparameter *moves* (read-from-string
                       (pre-process-input
                        (read-puzzle-input))))

(defparameter *initial-line* "abcdefghijklmnop")

(declaim (ftype (function (dance-line line-number) dance-line) s))
(defun s (line n)
  (declare (type dance-line line)
           (type line-number n))
  "Lifted from Alexandria's rotate-tail-to-head"
  (let* ((l (copy-seq line))
         (len (length line))
         (m (mod n len))
         (tail (subseq l (- len m))))
    (replace l l :start1 m :start2 0)
    (replace l tail)
    l))

(declaim (ftype (function (dance-line line-number line-number) dance-line) x))
(defun x (line x y)
  (declare (type dance-line line)
           (type line-number x y))
  (let ((l (copy-seq line)))
    (rotatef (aref l x)
             (aref l y))
    l))

(declaim (ftype (function (dance-line line-char line-char) dance-line) p))
(defun p (line x y)
  (declare (type dance-line line)
           (type line-char x y))
  (let ((l (copy-seq line)))
    (let ((pos-x (position x l))
          (pos-y (position y l)))
      (rotatef (aref l pos-x)
               (aref l pos-y))
      l)))

(declaim (ftype (function (dance-line cons) dance-line) dance))
(defun dance (line moves)
  (declare (type (simple-array character (16)) line)
           (type cons moves))
  (push line moves)
  (push '-> moves)
  (eval moves))

(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
     #'(lambda (&rest args)
         (multiple-value-bind 
               (result exists)
             (gethash args cache)
           (if exists
               result
               (setf (gethash args cache)
                     (apply fn args)))))))

;;; Solution, Part 1: "olgejankfhbmpidc"
;;; Solution, Part 2: "gfabehpdojkcimnl"
(defun solve (&key (line *initial-line*) (rounds 1))
  (let ((dance-m (memoize #'dance)))
    (loop repeat rounds
          do (setf line (funcall dance-m line *moves*))
          finally (return line))))
