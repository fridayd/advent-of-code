;;;; --- Day 18: Duet ---

(ql:quickload "cl-ppcre" :silent t)
(ql:quickload "cl-arrows" :silent t)
(use-package :cl-ppcre)
(use-package :cl-arrows)

(defparameter *input-file* "Day 18, Input.txt")

;; add suffix to instruction: rr, ri, ir, ii, r, or i?
(defun pre-process-line (line)
  "Wrap in parens: 'add a 2' -> '(add a 2)'. Add '*' to 'set' and 'mod'."
  (-<> line
       (regex-replace "(.*)" <> "(\\1)")))

(defun read-input-file (&optional (filename *input-file*))
  (with-open-file (in filename)
    (loop
      for line = (read-line in nil)
      while line
      collect (-> line
                  (pre-process-line)
                  (read-from-string)))))

(defun read-program (&optional (program-listing (read-input-file)))
  (make-array (length program-listing)
              :element-type 'cons
              :initial-contents program-listing))

;;; Interpreter

;;; Solution, Part 1: 3188
(defmacro with-registers ((hash-name) &body body)
  `(macrolet ((register (key)
                `(gethash ,key ,',hash-name 0)))
     (flet ((value-of (x)
              (if (symbolp x) (register x) x)))
         ,@body)))

;;(defmacro def-instruction (name &body body)
;;  `(defun ,name (x y registers)
;;     (with-registers (registers)
;;       ,@body)))

(defun set-instruction (x y registers)
  (with-registers (registers)
    (setf (register x) (value-of y))
    (incf (register 'ip)))
  :ok)

(defun add-instruction (x y registers)
  (with-registers (registers)
    (incf (register x) (value-of y))
    (incf (register 'ip)))
  :ok)

(defun mul-instruction (x y registers)
  (with-registers (registers)
    (setf (register x) (* (register x) (value-of y)))
    (incf (register 'ip)))
  :ok)

(defun mod-instruction (x y registers)
  (with-registers (registers)
    (setf (register x) (mod (register x) (value-of y)))
    (incf (register 'ip)))
  :ok)

(defun jgz-instruction (x y registers)
  (with-registers (registers)
    (if (< 0 (value-of x))
        (incf (register 'ip) (value-of y))
        (incf (register 'ip))))
  :ok)

(defun snd-instruction-1 (x y registers)
  (with-registers (registers)
    (setf (register 'sound) (value-of x))
    (incf (register 'ip)))
  :ok)

(defun rcv-instruction-1 (x y registers)
  (with-registers (registers)
    (when (/= 0 (value-of x))
      (if (gethash 'result registers)
          (register 'sound)
          (setf (register 'result)
                (register 'sound))))
    (incf (register 'ip)))
  :ok)

(defparameter *instruction-set-1* `((set . ,#'set-instruction)
                                    (add . ,#'add-instruction)
                                    (mul . ,#'mul-instruction)
                                    (mod . ,#'mod-instruction)
                                    (jgz . ,#'jgz-instruction)
                                    (snd . ,#'snd-instruction-1)
                                    (rcv . ,#'rcv-instruction-1)))

(defun debug-info (program registers)
  (with-registers (registers)
    (let* ((line (aref program (gethash 'ip registers)))
           (instruction-code (first line))
           (arg1 (second line))
           (arg2 (third line)))
      (list (register 'ip)
            (list instruction-code
                  arg1 (list (value-of arg1))
                  arg2 (list (value-of arg2)))))))

(defun one-step (program instruction-set registers)
  (let* ((ip (gethash 'ip registers))
         (line (aref program ip))
         (instruction-code (first line))
         (arg1 (second line))
         (arg2 (third line))
         (instruction-func (cdr (assoc instruction-code instruction-set))))
    (if (and (<= 0 ip) (< ip (length program)))
        (funcall instruction-func arg1 arg2 registers)
        :terminated)))

;; think this could be tightened up
(defun interpret-1 (&key
                      (program (read-program))
                      (instruction-set *instruction-set-1*)
                      (steps -1)
                      (debug nil))
  (let ((registers (make-hash-table))
        (debug-line nil)
        (debug-regs nil))
    (setf (gethash 'ip registers) 0)
    (do ((count 0 (1+ count))
         (status :ok (one-step program instruction-set registers))
         (result nil (gethash 'result registers)))
        ((or (= count steps)
             (not (equal status :ok))
             result)
         (list (list count) result))
      (when debug
        (setf debug-line (debug-info program registers))
        (setf debug-regs (loop for k being the hash-keys in registers
                               using (hash-value v)
                               collect (list k v)))
        (print (list debug-line debug-regs))))))

;;; Solution, Part 2: 7112

(defun snd-instruction-2 (x y registers)
  (with-registers (registers)
    (push (value-of x) (gethash 'out registers))
    (incf (register 'snd-count))
    (incf (register 'ip)))
  :ok)

(defun rcv-instruction-2 (x y registers)
  (with-registers (registers)
    (setf (register x) (pop (gethash 'in registers)))
    (if (register x)
        (progn (incf (register 'ip))
               :ok)
        :blocked)))

(defparameter *instruction-set-2* `((set . ,#'set-instruction)
                                    (add . ,#'add-instruction)
                                    (mul . ,#'mul-instruction)
                                    (mod . ,#'mod-instruction)
                                    (jgz . ,#'jgz-instruction)
                                    (snd . ,#'snd-instruction-2)
                                    (rcv . ,#'rcv-instruction-2)))

(defun run-to-stop (program instruction-set registers &key (debug 0))
  (loop for count = 1 then (1+ count)
        with buffer = nil
        while (eq :ok (one-step program instruction-set registers))
        do (when (< 1 debug)
             (print (debug-info program registers)))
        finally (rotatef buffer (gethash 'out registers))
                (when (< 0 debug)
                  (print `(:steps ,count)))
                (return (reverse buffer))))

(defun prepared-registers (number)
  (let ((registers (make-hash-table)))
    (setf (gethash 'ip registers) 0
          (gethash 'p registers) number)
    registers))

(defun solve2 (&key
                 (program (read-program))
                 (instruction-set *instruction-set-2*)
                 (steps 0)
                 (debug 0))
  (let* ((this-reg (prepared-registers 0))
         (that-reg (prepared-registers 1))
         (reg-1 that-reg) ; so we can count it's output
         (this->that nil))
    (setf this->that (run-to-stop program instruction-set this-reg))
    (loop
      for count = 1 then (1+ count)
      do (setf (gethash 'in that-reg) this->that)
         (rotatef this-reg that-reg)
         (setf this->that (run-to-stop program instruction-set this-reg :debug (1- debug)))
         (when (< 0 debug)
           (print `(:length-this->that ,(length this->that))))
      while (and (/= count steps)
                 this->that)
      finally (return `(:snd-count-1 ,(gethash 'snd-count reg-1))))))
