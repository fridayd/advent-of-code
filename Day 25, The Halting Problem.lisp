;;;; --- Day 25: The Halting Problem ---

(ql:quickload "arrow-macros")
(ql:quickload "cl-yaml")
(ql:quickload "cl-ppcre")

(defpackage aoc2017.day25
  (:use :cl :arrow-macros))

(in-package :aoc2017.day25)

(defparameter *cycles* 12134527)

;;; Prelude
(defun read-lines-from-file (filename)
  (with-open-file (in filename)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(defun parse-input (&optional (filename "input_day25.txt"))
  (->> (read-lines-from-file filename)
    (mapcar #'(lambda (line)
                (-<>> line
                  (cl-ppcre:regex-replace-all "([\\s-]*).*\\b(\\w+\\:?)\\.?" <> "\\1\\2")
                  (cl-ppcre:regex-replace-all "left" <> "-1")
                  (cl-ppcre:regex-replace-all "right" <> "1"))))
    (nthcdr 3)                ; skip the first 3 lines
    (format nil "~{~A~^~%~}") ; join all lines into one string
    (cl-yaml:parse)))

;;; Macine-running
(defun blank-tape (length)
  (make-array length :element-type 'bit :initial-element 0))

(defun run-handcoded (cycles &optional (tape (blank-tape (* 2 cycles))))
  "Run cycles, returns checksum (hand-coded implementation)."
  (declare (optimize (speed 3) (safety 0)))
  (let ((index cycles))
    (macrolet ((write-tape (value)
                 `(setf (aref tape index) ,value))
               (read-tape ()
                 `(aref tape index))
               (move (bit)
                 `(incf index (1- (* 2 ,bit))))
               (halt? ()
                 `(when (zerop (decf cycles))
                    (go HALT)))
               (state ((0-write 0-move 0-go) (1-write 1-move 1-go))
                 `(case (read-tape)
                    (0 (write-tape ,0-write) (move ,0-move) (halt?) (go ,0-go))
                    (1 (write-tape ,1-write) (move ,1-move) (halt?) (go ,1-go)))))
      (tagbody
       A (state (1 1 B) (0 0 C))
       B (state (1 0 A) (1 1 C))
       C (state (1 1 A) (0 0 D))
       D (state (1 0 E) (1 0 C))
       E (state (1 1 F) (1 1 A))
       F (state (1 1 A) (1 1 E))
       HALT)))
  (count 1 tape)) ; diagnostic checksum

;;; Solution, Part 1: 5593
(defun solve-25/1 (&optional (cycles 12134527))
  (run-handcoded cycles))

(defun run-interpreted (spec cycles &key (initial-state "A"))
  "Runs cycles, updating variables from the yaml produced nested hash table."
  (declare (optimize (speed 3) (safety 0)))
  (let ((tape (blank-tape (* 2 cycles))) ; an array twice the cycle count is ample.
        (cursor cycles)                  ; start in the middle of the tape
        (state initial-state))
    (loop repeat cycles
          for curr-value = (aref tape cursor)
          for (new-value step new-state) = (gethash curr-value (gethash state spec))
          do
             (setf (aref tape cursor) new-value)
             (incf cursor step)
             (setf state new-state))
    (count 1 tape)))

(defun state-name (state-name)
  (read-from-string state-name))

(defun case-forms (state-spec)
  (loop
    for key from 0 to 1
    for (new-value step new-state) = (gethash key state-spec)
    collect
    `(,key (setf (aref tape cursor) ,new-value)
           (incf cursor ,step)
           (go ,(state-name new-state)))))

(defun state-forms (spec)
  ;; sort keys first -> perfomance impact?
  (loop
    for state-tag being the hash-keys in spec using (hash-value state-spec)
    append
    `(,(state-name state-tag)
      (when (zerop (decf cycles))
        (go HALT))
      (case (aref tape cursor) ,@(case-forms state-spec)))))

(defun machine-from-spec (spec &key (initial-state "A"))
  "Builds a lambda that takes a cycle count and runs the spec'd machine using a tagbody."
  `(lambda (cycles)
     (declare (optimize (speed 3) (debug 0) (safety 0)))
     (let ((tape (blank-tape (* 2 cycles)))
           (cursor cycles))
       (tagbody
          (go ,(state-name initial-state))
          ,@(state-forms spec)
        HALT)
       (count 1 tape))))

(defun compile-and-run (spec cycles)
  "Run cycles, returns checksum (generates and compiles function at runtime)."
  (-<>> (machine-from-spec spec)
        (compile nil)
        (funcall <> cycles)))

;;; Solution, Part 2: There is no Part 2. :-)
