;;;; --- Day 7: Recursive Circus ---

;;; Sample Input
;;; "qdxpkx (67)" returns (NIL "qdxpkx")
;;; "wubltb (276) -> udcok, pihpjbp, hiifqwb" returns ("wubltb" "udcok" "pihpjbp" "hiifqwb")

(defparameter *puzzle-input-file* "Day 7, Input.txt")
(defparameter *sample-input-file* "Day 7, Sample Input.txt")

;;; Part 1
;;; Solution: xegshds

(ql:quickload "cl-ppcre")

(defun read-puzzle-input-file (&optional (filename *puzzle-input-file*))
  (with-open-file (in filename)
    (loop
      for line = (read-line in nil)
      while line
      collect line)))

(defun extract-parents (line)
  (let ((words (cl-ppcre:all-matches-as-strings "(\\w+)" line)))
    (if (< 2 (length words))
        (first words)
        nil)))

(defun extract-children (line)
  (let ((words (cl-ppcre:all-matches-as-strings "(\\w+)" line)))
    (if (< 2 (length words))
        (cddr words)
        nil)))

(defun find-root-node (&optional (input-lines (read-puzzle-input-file)))
  (let*  ((parents (remove nil (mapcar #'extract-parents input-lines)))
          (children (remove nil (reduce #'append input-lines :key #'extract-children)))
          (root-nodes (set-difference parents
                                      children
                                      :test #'equal)))
    (car root-nodes)))


;;; Part 2
;;; Solution: 299

(ql:quickload "alexandria")

(defparameter *node-table* (make-hash-table :test 'equal))
(defparameter *parent-table* (make-hash-table :test 'equal))
(defparameter *child-table* (make-hash-table :test 'equal))

(defun weight-of (node)
  (gethash node *node-table*))

(defun parent-of (node)
  (gethash node *child-table*))

(defun children-of (node)
  (gethash node *parent-table*))

(defun siblings-of (node)
  (remove node
          (children-of (parent-of node))))

(defun read-input (&optional (input-lines (read-puzzle-input-file)))
  "Fill  in the three data structures: *node-table*, *parent-table*, and *children*."
  (loop
    for line in input-lines
    do
       (let* ((words (cl-ppcre:all-matches-as-strings "(\\w+)" line))
              (node-name (first words))
              (node-weight (parse-integer (second words)))
              (children (cddr words)))
         (setf (gethash node-name *node-table*) node-weight)
         (when children
           (setf (gethash node-name *parent-table*) children)
           (dolist (child children)
             (setf (gethash child *child-table*) node-name))))))

(defun root-node (&optional (read-input-fn #'read-input))
  "Finds the root node—the one node with children but isn't any other node's child."
  (funcall read-input-fn)
  (let ((parents (alexandria:hash-table-keys *parent-table*))
        (children (alexandria:hash-table-keys *child-table*)))
    (first (set-difference parents
                           children
                           :test #'equal))))

;; Could memoize this
(defun tree-weight (node)
  "Recursively find the weight of a tree, from node down."
  (let ((weight (weight-of node))
        (children (children-of node)))
    (if children
        (+ weight
           (reduce #'+
                   children
                   :key #'tree-weight))
        weight)))

(defun only-different-number-p (x items)
  "Is x the only different number?"
  (let ((others (remove x items)))
    (when (= (length others)
             (1- (length items)))
      (and (/= x (first others))
           (samep others)))))

(defun the-one-different-number (items)
  "Find the one different number, or nil if there isn't (just) one."
  (dolist (x items)
    (when (only-different-number-p x items)
      (return x))))

;; Assumes we start at, or on the root-ward side of the wrongly weighted node.
;; (The root node is a good node to start at.)
(defun foo (&optional (node (root-node)))
  "Find the wrongly-weighted node, and return the weight it should have."
  (let* ((children (children-of node))
         (child-tree-weights (mapcar #'tree-weight children)))
    (break)
    (if (apply #'= child-tree-weights) ;; are the children balanced?
        ;; ...so this node must be the 'wrong' weight node...
        (let ((sub-tree-weight (reduce #'+ child-tree-weights))
              (sibling-tree-weight (tree-weight (first (siblings-of node)))))
          ;; ...and this node should be equal to:
          (- sibling-tree-weight sub-tree-weight))
        ;; or we recurse on the odd child
        (let* ((children-and-weights (pairlis children child-tree-weights))
               (odd-weight (the-one-different-number child-tree-weights))
               (odd-child (car (rassoc odd-weight children-and-weights))))
          (foo odd-child)))))
