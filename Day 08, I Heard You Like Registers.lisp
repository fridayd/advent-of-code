;;;; --- Day 8: I Heard You Like Registers ---

(defparameter *puzzle-input* "Day 8, Input.txt")

(defun read-input-file (&optional (filename *puzzle-input*))
  (with-open-file (in filename)
    (loop
      for line = (read-line in nil)
      while line
      collect line)))

;;; First line of input:
;;; "aj dec -520 if icd < 9"

(ql:quickload "cl-ppcre")
(ql:quickload "alexandria")

;;; Part 1
;;; Solution: 5075

;;; Part 2
;;; Solution: 7310

(defparameter *registers* (make-hash-table))
(defparameter *max-value* 0)

(defun max-register-value ()
  (reduce #'max (alexandria:hash-table-values *registers*)))

(defun inc (register amount)
  (incf (gethash register *registers* 0) amount))

(defun dec (register amount)
  (decf (gethash register *registers* 0) amount))

(setf (fdefinition '==) #'=)
(setf (fdefinition '!=) #'/=)

(defun pre-process-line (line)
  (let* ((step1 (cl-ppcre:regex-replace-all "^\\w+|(?<=if )\\w+"
                                            line
                                            "'\\&"))
         (step2 (cl-ppcre:regex-replace ".*"
                                        step1
                                        "(register-instruction \\&)")))
    step2))

(defmacro register-instruction (target
                                change
                                amount
                                if!
                                reference
                                comparison
                                value)
  (declare (ignore if!))
  `(if (,comparison (gethash ,reference *registers* 0) ,value)
       (,change ,target ,amount)))

(defun run-input (&optional (program-lines (read-input-file)))
  (dolist (line program-lines)
    (eval (read-from-string (pre-process-line line)))
    (setf *max-value* (max *max-value* (max-register-value)))))

(defun largest-register-value-after-program-run ()
  (run-input)
  (reduce #'max (alexandria:hash-table-values *registers*)))

(defun max-register-value-while-running ()
  (run-input)
  *max-value*)
