;;;; --- Day 10: Knot Hash ---

;; 4. add function to render dense hash from sparse hash
;; 6. add function that converts final rope into a string of base16 digits

(defparameter *puzzle-input* "157,222,1,2,177,254,0,228,159,140,249,187,255,51,76,30")

(ql:quickload "cl-ppcre")

(defparameter *puzzle-input-1* (mapcar #'parse-integer
                                       (cl-ppcre:split ","
                                                       *puzzle-input*)))

(defun ascii-codes (string)
  (loop
    for c across string
    collect (char-code c)))

(defparameter *hash-input-suffix* '(17 31 73 47 23))

(defparameter *puzzle-input-2* (append (ascii-codes *puzzle-input*)
                                       *hash-input-suffix*))

;;; Core algorithm

(defun iota (count &optional (start 0) (step 1))
  (loop repeat count
        for i from start by step
        collect i))

(defun circularize (list)
  (setf (cdr (last list))
        list))

;; State

(defparameter *rope* (circularize (iota 256)))
(defparameter *rope-start* *rope*)
(defparameter *skip* 0)

(defun reverse-segment-and-advance (n)
  (let ((rev-seq '()))
    ;; build reverse sequence
    (dotimes (i n)
      (push (nth i *rope*) rev-seq))
    ;; in-place, set reversed values
    (dotimes (i n)
      (setf (nth i *rope*) (nth i rev-seq))))
  ;; advance rope n places
  (setf *rope* (nthcdr (+ n *skip*) *rope*))
  (incf *skip*))

(defun knot-hash (input &key (rounds 64))
  (dotimes (r rounds)
    (dolist (n input)
      (reverse-segment-and-advance n))))

(defun block-of-16 (list)
  (loop
    for i from 0 to 15
    collect (nth i list)))

(defun hash-blocks ()
  (setf *rope* *rope-start*)
  (loop
    for n from 1 to 16
    collect (block-of-16 *rope*)
    do (setf *rope*
             (nthcdr 16
                     *rope*))))

(defun condense (sparse-byte)
  (reduce #'logxor sparse-byte))

(defun base16-string (list-of-numbers)
  (format nil "~{~(~2,'0x~)~}" list-of-numbers))

(defun output ()
    (base16-string (mapcar #'condense
                           (hash-blocks))))

;;; Solution functions

;;; Answer, Part 1: 62238
(defun solve-part-1 ()
  (let ((first-value *rope*)
        (second-value (cdr *rope*)))
    (knot-hash *puzzle-input-1* :rounds 1)
    (* (car first-value) (car second-value))))

;;; Answer, Part 2: 2b0c9cc0449507a0db3babd57ad9e8d8
(defun solve-part-2 ()
  (knot-hash *puzzle-input-2*)
  (output))
