;;;; --- Day 15: Dueling Generators ---

(defparameter *divisor* 2147483647)
(defparameter *multiple-a* 16807)
(defparameter *factor-a* 4)
(defparameter *seed-a* 783)
(defparameter *multiple-b* 48271)
(defparameter *factor-b* 8)
(defparameter *seed-b* 325)

;;;; Solution, Part 1: 650

(defun low16 (n)
  (ldb (byte 16 0) n))

(defun gen-gen-1 (seed multiple)
  (lambda (&optional (n seed))
    (rem (* n multiple) *divisor*)))

(defparameter *gen-a-1* (gen-gen-1 *seed-a* *multiple-a*))
(defparameter *gen-b-1* (gen-gen-1 *seed-b* *multiple-b*))

(defun score (a-gen b-gen &key (m-rounds 40))
  (let ((rounds (* m-rounds 1000000)))
    (loop repeat rounds
          for a = (funcall a-gen) then (funcall a-gen a)
          for b = (funcall b-gen) then (funcall b-gen b)
          if (eq (low16 a)
                 (low16 b))
            count t)))

(ql:quickload "iterate" :silent t)
(use-package :iter)

(defun score-iter (a-gen b-gen &key (m-rounds 40))
  (let ((rounds (* m-rounds 1000000)))
    (iterate (repeat rounds)
      (for a first (funcall a-gen) then (funcall a-gen a))
      (for b first (funcall b-gen) then (funcall b-gen b))
      (if (eq (low16 a)
              (low16 b))
          (count t)))))

;;;; Solution, Part 2: 336

(defun divisorp (n d)
  (zerop (rem n d)))

(defmacro gen-gen-2 (gen-name seed gen-1 factor)
  `(defun ,gen-name (&optional (n ,seed))
     (let ((next (funcall ,gen-1 n)))
       (if (divisorp next ,factor)
           next
           (,gen-name next)))))

(defun gen-a-2 (&optional (n *seed-a*))
  (let ((next (funcall *gen-a-1* n)))
    (if (divisorp next *factor-a*)
        next
        (gen-a-2 next))))

(defun gen-b-2 (&optional (n *seed-b*))
  (let ((next (funcall *gen-b-1* n)))
    (if (divisorp next *factor-b*)
        next
        (gen-b-2 next))))
