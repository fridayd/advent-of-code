;;;; --- Day 6: Memory Reallocation ---

(defparameter *puzzle-input* '(4 1 15 12 0 9 9 5 5 8 7 3 14 5 12 3))

;;; Part 1
;;; Solution: 6681
(defparameter *test-input-1* '(0 2 7 0))

(defparameter *seen-allocations* '())

(defun seen (allocation)
  "Remember this allocation and return T if it was seen before, nil otherwise."
  (push (copy-seq allocation) *seen-allocations*)
  (member allocation (cdr *seen-allocations*) :test #'equalp))

(defun circularize (in)
  (setf (cdr (last in)) in))

(defun next-allocation (allocation)
  (let* ((bank-list (mapcar #'list allocation))
         (bank-loop (member (reduce #'max bank-list :key #'car)
                            (circularize (copy-list bank-list))
                            :key #'car))
         (blocks-to-distribute (caar bank-loop)))
    (setf (caar bank-loop) 0)
    (loop
      repeat blocks-to-distribute
      do
         (setf bank-loop (rest bank-loop))
         (incf (caar bank-loop))
         ;(break)
      finally
         (return (mapcar #'car bank-list)))))

(defun cycles-till-repeated-allocation (initial-allocation)
  (loop for allocation = initial-allocation then (next-allocation allocation)
        and steps = 0 then (1+ steps)
        while (not (seen allocation))
        finally (return steps)))

;;; Part 2
;;; Solution: 2392
(defun length-of-inifinite-loop (initial-allocation)
  (let ((step-at-cycle-end (1+ (cycles-till-repeated-allocation initial-allocation)))
        (step-at-cycle-start (length (member (first *seen-allocations*)
                                             (rest *seen-allocations*)
                                             :test #'equalp))))
    (- step-at-cycle-end
       step-at-cycle-start)))
