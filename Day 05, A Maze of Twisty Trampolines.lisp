;;;; --- Day 5: A Maze of Twisty Trampolines, All Alike ---

(defparameter *puzzle-input-file* "Day 5, Input.txt")

(defun read-puzzle-input (filename)
  (with-open-file (in filename)
    (loop
      for line = (read in nil)
      while line
      collect line)))

(defun puzzle-input ()
  (let ((in (read-puzzle-input *puzzle-input-file*)))
    (make-array (length in) :initial-contents in)))

;;; Part 1
;;; Solution: 374269

(defun steps-to-exit (maze)
  (let ((upper-bound (1- (array-dimension maze 0)))
        (lower-bound 0))
    (loop for place = 0 then (+ place jump)
          for jump = (aref maze place) then (aref maze place)
          do
             (incf (aref maze place))
          counting place into steps
          while (<= lower-bound (+ place jump) upper-bound)
          finally (return steps))))

;;; Part 2
;;; Solution: 27720699

(defun steps-to-exit-2 (maze)
  (let ((upper-bound (1- (array-dimension maze 0)))
        (lower-bound 0))
    (loop for place = 0 then (+ place jump)
          for jump = (aref maze place) then (aref maze place)
          do (if (<= 3 jump)
                 (decf (aref maze place))
                 (incf (aref maze place)))
          counting place into steps
          while (<= lower-bound (+ place jump) upper-bound)
          finally (return steps))))
