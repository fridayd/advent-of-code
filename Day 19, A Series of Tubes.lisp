;;;; --- Day 19: A Series of Tubes ---

(defparameter *input-file* "Day 19, Input.txt")

(defun read-file-into-list-of-lines (file)
  (with-open-file (in file)
    (loop for line = (read-line in nil)
          while line
          collect (coerce line 'list))))

(defun read-puzzle-input()
  (2d-array-from-list-of-lines
   (read-file-into-list-of-lines *input-file*)))

;; Solution, Part 1: LXWCKGRAOY
;; Solution, Part 2: 17302

;; need to set starting 'here' first
(defun day-19-1 ()
  (let* ((map (read-file-into-list-of-lines *input-file*))
         (here (list 0 (position #\| (first map))))
         (heading '(1 0)))
    (labels ((at (location)
               (nth (second location)
                    (nth (first location) map)))
             (look (direction)
               (mapcar #'+ here direction))
             (move-to (direction)
               (setf here (look direction)))
             (turn (direction)
               (setf heading direction))
             (aside ()
               (reverse heading))
             (other-way ()
               (mapcar #'- (aside)))
             (blankp (symbol)
               (eq symbol #\ ))
             (pathp (symbol)
               (not (blankp symbol)))
             (turnp (symbol)
               (eq symbol #\+))
             (linep (symbol)
               (or (eq symbol #\|)
                   (eq symbol #\-)))
             (output (seen)
               (format nil "~{~A~}" (reverse seen))))
      (loop
        with letters-seen = nil
        with steps = 0
        do
           (move-to heading)
           (incf steps)
           (cond
             ((linep (at here)) nil)
             ((turnp (at here)) (if (pathp (at (look (aside))))
                                    (turn (aside))
                                    (turn (other-way))))
             ((blankp (at here)) (return (list steps (output letters-seen))))
             (t (push (at here) letters-seen)))))))
