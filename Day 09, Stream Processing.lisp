;;;; --- Day 9: Stream Processing ---

;;; Part 1 Solution: 10,820
;;; Part 2 Solution:  5,547

(defparameter *puzzle-input* "Day 9, Input.txt")

(defun char-processor ()
  (let ((state 'grouping)
        (depth 0))
    #'(lambda (c)
        (case state
          ;; in every case, return the score for this char
          (grouping (case c
                      (#\{ (incf depth) 0)
                      ;; (1+ depth) for score; 0 for counting garbage
                      (#\} (decf depth) 0)
                      (#\! (setf state 'grouping-skip) 0)
                      (#\< (setf state 'garbage) 0)
                      (t 0)))
          (grouping-skip (setf state 'grouping) 0)
          (garbage (case c
                     (#\! (setf state 'garbage-skip) 0)
                     (#\> (setf state 'grouping) 0)
                     ;; 0 for score; 1 for counting garbage
                     (t 1)))
          (garbage-skip (setf state 'garbage) 0)))))

(defun read-input-file (&optional (filename *puzzle-input*))
  (with-open-file (in filename)
    (let ((process-char (char-processor)))
      (loop
        for c = (read-char in nil)
        while c
        summing (funcall process-char c) into score
        finally (return score)))))
