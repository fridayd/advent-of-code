;;;; --- Day 20: Particle Swarm ---

(eval-when (:compile-toplevel)
  (ql:quickload "cl-ppcre")
  (ql:quickload "cl-arrows"))
;(use-package :cl-arrows)

;;; input file: "input_day20.txt"

(defun read-lines-from-file (input-file)
  (with-open-file (in input-file)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(defun group-by-axis (list)
  (loop for i from 0 to 2
        collect (list (nth (+ 0 i) list)
                      (nth (+ 3 i) list)
                      (nth (+ 6 i) list))))

;; TD: Is an array still the best choice?
(defun load-input ()
  ;(make-array 1000 :element-type 'cons :initial-contents
              (loop for line in (read-lines-from-file "input_day20.txt")
                    collect (->> line
                                 (cl-ppcre:all-matches-as-strings "[-\\d]+")
                                 (mapcar #'parse-integer)
                                 (group-by-axis))))

;;; Solution, Part 1: 150
(defun day20-1 ()
  (labels ((manhattan-distance (vector)
             (reduce #'+ vector :key #'abs))
           (min-acceleration (a b)
             (if (< (manhattan-distance (mapcar #'third a))
                    (manhattan-distance (mapcar #'third b)))
                 a
                 b)))
    (let ((particles (load-input))) 
      (position (reduce #'min-acceleration particles) particles))))

;;; Solution, Part 2:

(defun 1d-position-at (time pva)
  "1D position of given particle at given time"
  (destructuring-bind (initial-position initial-velocity acceleration) pva
    (+ initial-position
       (* time initial-velocity)
       (* time (+ time 1) acceleration (/ 2)))))

(defun 3d-position-at (time particle)
  "3D position of given particle at given time"
  (mapcar (lambda (p) (1d-position-at time p))
          particle))

(defun quadratic-terms (pva)
  "Returns a, b, c terms for the quadratic equation"
  (destructuring-bind (position velocity acceleration) pva
    (list acceleration
          (+ acceleration (* 2 velocity))
          (* 2 position))))

(defun quadratic-solutions (terms)
  "Returns solutions, if any"
  (destructuring-bind (a b c) terms
    (cond ((not (zerop a))
           (let ((square-root (sqrt (- (expt b 2) (* 4 a c))))
                 (denominator (* 2 a)))
             (list (/ (+ (- b) square-root) denominator)
                   (/ (- (- b) square-root) denominator))))
          ((not (zerop b)) (list (/ (- c) b)))
          ((not (zerop c)) (list c)))))

(defun valid-solution-p (solution)
  "Is solution a positive integer?"
  (and (integerp solution)
       (< 0 solution)))

(defun 1d-collision-times (a b)
  "Considering each axis in turn, return collision times for two particles, if any."
  (->> (mapcar #'- a b)
       (quadratic-terms)
       (quadratic-solutions)
       (remove-if-not #'valid-solution-p)))

(defun 4d-collision-loci (a b)
  "Given two particles, return their collision times and places, if any."
  (->> (mapcar #'1d-collision-times a b)
       (reduce #'intersection)
       ; a collision locus is a list: (time x y z)
       (mapcar (lambda (time) (cons time (3d-position-at time a))))))

(defun all-pairings (list)
  "Return all possible pairs of elements of given list"
  (loop for (this . those) on list append
    (loop for that in those collect
      (list this that))))

(defmacro add-to (place to-add)
  "Expand to setf a to be (union a b)"
  `(setf ,place (union ,place ,to-add)))

(defun all-collisions (pairs)
  "Return hash-table of 4d-collision-locus:colliding-particles"
  (let ((all-collisions (make-hash-table :test #'equal)))
    (loop for (a b) in pairs do
      (loop for locus in (4d-collision-loci a b) do
        (add-to (gethash locus all-collisions)
                (list a b))))
    all-collisions))

(defun gather-dead (dead colliders)
  "If at least two colliders haven't died previously, their collision will happen."
  (let* ((already-dead (intersection dead colliders))
         (still-to-die (- (length colliders)
                          (length already-dead))))
    (if (< 1 still-to-die)
        (union dead colliders)
        dead)))

(defun condense-collisions (collisions)
  "Eariler collisions may preclude later ones; account for this."
  (-<>> collisions
        (alexandria:hash-table-keys)
        (sort <> #'< :key #'first) ; time is the first element of a collision locus
        (mapcar (lambda (locus) (gethash locus collisions)))
        (reduce #'gather-dead)))

;;; Answer: 657
(defun day20-2b ()
  (let* ((particles (load-input))
         (dead (-<>> particles
                     (all-pairings)
                     (all-collisions)
                     (condense-collisions))))
    (- (length particles)
       (length dead))))
