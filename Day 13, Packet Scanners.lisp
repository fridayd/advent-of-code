;;;; --- Day 13: Packet Scanners ---

(defparameter *puzzle-input* "Day 13, Input.txt")

(defun read-input (&optional (file *puzzle-input*))
  (with-open-file (in file)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(ql:quickload "cl-ppcre")

(defun parse-line (line)
  (let* ((strings (cl-ppcre:split ":" line))
         (nums (mapcar #'parse-integer strings)))
    (cons (first nums) (second nums))))

(defun severity (layer)
  (* (car layer) (cdr layer)))

(defun caught-p (layer delay)
  (let* ((n (car layer))
         (depth (cdr layer))
         (scan-length (+ depth (- depth 2))))
    (zerop (mod (+ n delay) scan-length))))

(defun caught-layers (layers delay)
  (remove-if-not #'(lambda (layer)
                     (caught-p layer delay))
                 layers))

(defun trip-severity (layers &optional (delay 0))
  (reduce #'+ (caught-layers layers delay) :key #'severity))

;;; Solution, Part 1: 632
(defun solve-part1 ()
  (let ((layers (mapcar #'parse-line (read-input))))
    (trip-severity layers)))


;;; Solution, Part 2: 3849742
(defun solve-part2 ()
  (let ((layers (mapcar #'parse-line (read-input))))
    (loop for delay = 0 then (1+ delay)
          until (zerop (length (caught-layers layers delay)))
          finally (return delay))))
