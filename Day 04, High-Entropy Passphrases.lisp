;;;; Day 4: High-Entropy Passphrases

(ql:quickload "split-sequence")

(defparameter *puzzle-input-file* "Day 4, Input.txt")

(defun read-puzzle-input (filename)
  (with-open-file (in filename)
    (loop
      for line = (read-line in nil)
      while line
      collect (split-sequence:split-sequence #\Space line))))

(defun puzzle-input ()
  (read-puzzle-input *puzzle-input-file*))

;;; Part 1
;;; Solution: 455

(defun count-valid-passphrases ()
  (let ((phrases (puzzle-input)))
    (loop for p in phrases
          if (equal p (remove-duplicates p))
            counting p)))

;;; Part 2
;;; Solution: 186

(defun anagram-pair-p (a b)
  "Are these two strings anagrams of each other?"
  (let ((sorted-a (sort a #'char<))
        (sorted-b (sort b #'char<)))
    (equal sorted-a sorted-b)))

(defun count-valid-passphrases-2 ()
  (let ((phrases (puzzle-input)))
    (loop for p in phrases
          if (equal p (remove-duplicates p :test #'anagram-pair-p))
            count p)))
