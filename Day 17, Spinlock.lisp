;;;; --- Day 17: Spinlock ---

(declaim (optimize (speed 3) (safety 0)))

(defparameter *puzzle-input* 354)

(setf *print-circle* T)

(defun circularize (list)
  (setf (cdr (last list))
        list))

(defun spin (lock)
  (declare (type cons lock))
  (setf lock (nthcdr *puzzle-input* lock)))

(defun insert (lock n)
  (declare (type cons lock)
           (type (integer 0 50000000) n))
  (setf (cdr lock) (cons n (cdr lock))))

;;; Solution, Part 1: 2000
(defun solve1 ()
  (let ((spinlock (circularize (list 0))))
    (loop for i from 1 to 2017
          do
             (setf spinlock (spin spinlock))
             (setf spinlock (insert spinlock i))
          finally (return (cadr spinlock)))))

;;; Solution, Part 2: 10242889
(defun solve2 (&key (rounds 50000000))
  (declare (type (integer 0 50000000) rounds))
  (let* ((spinlock (circularize (list 0)))
         (zeroth spinlock))
     (loop for i of-type (integer 0 50000000)from 1 to rounds
           do
              (setf spinlock (spin spinlock))
              (setf spinlock (insert spinlock i))
           finally (setf (cdr spinlock) nil) ; suspect this speeds up GC
                   (return (cadr zeroth)))))
