;;;; Day 22: Sporifica VirusSporifica Virus

(eval-when (:compile-toplevel)
  (ql:quickload "alexandria")
  (ql:quickload "arrow-macros")
  (use-package :arrow-macros))

;;; Grid (hash-table) values:
;;;   # = infected
;;;   F = flagged
;;;   W = weakened
;;;   nil = clean
;;; The starting position, the center of the input grid, is the origin: (0,0).
;;; Y-axis is reversed: (- -) is upper left, (+ +) is lower right.
;;; Direction is a 2d unit vector; Add it to a location to move that way.

(defun lines-from-file (file-name)
  (with-open-file (in file-name)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(defun grid-from-input (&optional (file-name "input_day22.txt"))
  (let* ((input-lines (lines-from-file file-name))
         (input-height (length input-lines))
         (input-width (length (first input-lines)))
         (start-x (- (round (/ input-width 2))))
         (start-y (- (round (/ input-height 2))))
         (grid (make-hash-table :test 'equalp)))
    (loop for row in input-lines
          and y = start-y then (1+ y) do
            (loop for char across row
                  and x = start-x then (1+ x)
                  when (eq char #\#)
                    do (setf (gethash (list x y) grid) #\#)))
    grid))

;;; Solution, Part 1: 5182
(defun solve-part1 (grid n)
  (labels ((left (d)
             (list (second d) (- (first d))))
           (right (d)
             (list (- (second d)) (first d))))
    (loop repeat n
          with here = '(0 0)
          with direction = '(0 -1)
          count (not (gethash here grid)) into new-infections
          do
             (if (gethash here grid)
                 (progn
                   (remhash here grid)
                   (setf direction (right direction)))
                 (progn
                   (setf (gethash here grid) #\#)
                   (setf direction (left direction))))
             (setf here (mapcar #'+ here direction)) ; move in direction
          finally
             (return (list grid here new-infections)))))

;;; Solution , Part 2: 2512008
(defun solve-part2 (grid n)
  (labels ((left (d)
             (list (second d) (- (first d))))
           (right (d)
             (list (- (second d)) (first d)))
           (backward (d)
             (mapcar #'- d)))
    (macrolet ((turn (from to)
                 `(setf ,from (,to ,from))))
      (loop repeat n
            with here = '(0 0)
            with direction = '(0 -1)
            with state = (gethash here grid #\.)
            count (eq state #\W) into new-infections
            do
               (case state
                 (#\. (setf (gethash here grid) #\W))
                 (#\W (setf (gethash here grid) #\#))
                 (#\# (setf (gethash here grid) #\F))
                 (#\F (remhash here grid)))
               (case state
                 (#\. (turn direction left))
                 (#\W)
                 (#\# (turn direction right))
                 (#\F (turn direction backward)))
               (setf here (mapcar #'+ here direction))
               (setf state (gethash here grid #\.))
            finally
               (return (list grid here new-infections))))))

;;;
;;; Develop and Debug
(defun sample-grid ()
  "Returns starting grid from example on AoC 2017 web page."
  (let ((grid (make-hash-table :test #'equalp)))
    (setf (gethash '(-1 0) grid) #\#)
    (setf (gethash '(1  -1) grid) #\#) ; reversed y coordinate (to match my input reading)
    grid))

(defun largest-value (grid)
  "Returns the largest absolute value among the grid's keys."
  (-<>> (alexandria:hash-table-keys grid)
        (reduce #'append)
        (reduce #'max <> :key #'abs)))

(defun print-grid (grid &optional (location '(0 0)) (infections nil))
  (let ((max (1+ (largest-value grid))))
    (loop for y from (- max) to max do
      (loop for x from (- max) to max
            for here = (list x y)
            for state = (gethash here grid #\.)
            do
               (if (equalp here location)
                   (progn (princ #\[)
                          (princ state)
                          (princ #\]))
                   (progn (princ #\Space)
                          (princ state)
                          (princ #\Space))))
      (princ #\Newline)))
  (values infections))
