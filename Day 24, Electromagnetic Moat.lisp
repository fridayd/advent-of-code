;;;; --- Day 24: Electromagnetic Moat ---

(ql:quickload "cl-ppcre")
(use-package :cl-ppcre)
(ql:quickload "arrow-macros")
(use-package  :arrow-macros)

(defun read-lines-from-file (filename)
  (with-open-file (in filename)
    (loop for line = (read-line in nil)
          while line
          collect line)))

;;; Bridge Components are just twe-element lists, like "(3 4)".
(defun components-from-input (&optional (input-file "input_day24.txt"))
  (loop for line in (read-lines-from-file input-file)
        collect (->> line
                  (all-matches-as-strings "\\d+")
                  (mapcar #'parse-integer))))

(defun other-end (this-end component)
  (if (/= this-end (first component))
      (first component)
      (second component)))

(defun matching (n)
  "Return a boolean function for 'component has a side with _n_ ports'"
  (lambda (component)
    (or (= n (first component))
        (= n (second component)))))

(defun strength (component)
  (reduce #'+ component))

;;; TD: delete this
(defun adding-strength-of (component)
  (lambda (s) (+ s (strength component))))

;;; Bridge Summaries are a paired strength and length of a subsection of bridge.
(defun empty-summary ()
  (list (cons 0 0)))

(defun bridge-strength (bridge-summary)
  (car bridge-summary))

(defun bridge-length (bridge-summary)
  (cdr bridge-summary))

(defun adding-to-summary (component)
  "Return a function that updates a bridge summary with the given component."
  (lambda (bridge-summary)
    (cons (+ (bridge-strength bridge-summary) (strength component))
          (+ (bridge-length bridge-summary) 1))))

(defun possible-bridges (n components)
  "Recursively builds a list of possible bridges, summarized by strength and length."
  (let* ((these-components (remove-if-not (matching n) components)))
    (if these-components
        (loop for this-component in these-components
              for next-components = (remove this-component components)
              for next-n = (other-end n this-component)
              nconc (mapcar (adding-to-summary this-component)
                            (possible-bridges next-n
                                              next-components)))
        (empty-summary))))

;;; Solution, Part 1: 1906
(defun solve-part1 ()
  "What is the strongest bridge we can make?"
  (-<>> (possible-bridges 0 (components-from-input))
        (reduce #'max <> :key #'bridge-strength)))

;;; Solution, Part 2: 1824
(defun solve-part2 ()
  "How strong is the longest bridge we can make?"
  (let* ((bridges (possible-bridges 0 (components-from-input)))
         (max-length (reduce #'max bridges :key #'bridge-length)))
    (-> (find max-length bridges :key #'bridge-length)
        (bridge-strength))))
