;;;; --- Day 23: Coprocessor Conflagration ---

(eval-when (:compile-toplevel)
  (ql:quickload "cl-ppcre")
  (ql:quickload "arrow-macros")
  (use-package :arrow-macros))

;;; Prelude
(defun read-lines-from-file (filename)
  (with-open-file (in filename)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(defun wrap-line (line)
  (cl-ppcre:regex-replace "(.*)" line "(\\1)"))

(defun read-program (filename)
  (let* ((listing (->> (read-lines-from-file filename)
                    (mapcar #'wrap-line)
                    (mapcar #'read-from-string))))
    (make-array (length listing)
                :element-type 'cons
                :initial-contents listing)))

;;; Execution
(defmacro mulf (place multiplier)
  `(setf ,place (* ,place ,multiplier)))

(defun dispatch-instruction (instruction registers)
  "Returns modified registers and relative jump (1 for a non-jmp instruction)."
  (flet ((arg-value (arg)
           (if (symbolp arg)
               (gethash arg registers 0)
               arg)))
    (destructuring-bind (op arg1 arg2) instruction
       (values
         (case op
           (set (setf (gethash arg1 registers 0) (arg-value arg2)) 1)
           (sub (decf (gethash arg1 registers 0) (arg-value arg2)) 1)
           (mul (mulf (gethash arg1 registers 0) (arg-value arg2)) 1)
           (jnz (if (/= 0 (arg-value arg1))
                    (arg-value arg2)
                    1)))
         registers))))

;;; Solution, Part 1: 6241
(defun run (program)
  "Runs program until it faults by trying to jump out of itself."
  (let ((registers (make-hash-table))
        (maximum-ip (1- (length program))))
    (loop for ip = 0 then (+ ip jmp)
          for jmp = (dispatch-instruction (aref program ip) registers)
          while (<= 0 (+ ip jmp) maximum-ip)
          count (eql 'mul (first (aref program ip))))))

(defun rewritten-program ()
  "Hand re-written (de-complied?) input program."
  (let ((a 1)
        (c 125000)
        (h 0))
    (loop for b = 108100 then (+ b 17)
          until (= b c) do
            (loop for d from 2 to b do
              (loop for e from 2 to b do
                (when (= b (* d e))
                  (setf f 0))))
            (when (= f 0)
              (incf h)))))

;;; Solution, Part 2: 909
(defun solve-part2 ()
  "Hand optimized re-write of the input program, when a=1."
  (flet ((nonprimep (n)
           "Retun t if n is nonprime (composite), nil otherwise."
           (loop for factor from 2 below n
                 thereis (zerop (mod n factor)))))
    (loop for x from 108100 to 125100 by 17 ; below or upto?
          count (nonprimep x))))

;;; Development
(defun dump-regs (registers)
  (maphash (lambda (reg value) (print (list reg value)))
           registers)
  (values))
