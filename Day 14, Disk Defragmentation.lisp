;;;; --- Day 14: Disk Defragmentation ---

;;;; Knot Hash funcions (adapted from Day 10)

;;; Knot Hash Input
(defparameter *hash-input-suffix* '(17 31 73 47 23))

(defun ascii-codes (string)
  (loop
    for c across string
    collect (char-code c)))

(defun prep-hash-input (input)
  (typecase input
    (list input)
    (string (append (ascii-codes input)
                    *hash-input-suffix*))))

;;; Knot Hash Algorithm

;; Prelude
(defun iota (count &optional (start 0) (step 1))
  (loop repeat count
        for i from start by step
        collect i))

(defun circularize (list)
  (setf (cdr (last list))
        list))

;; Core
(defun reverse-first-n-segment (n rope)
  (let ((rev-seq '()))
    (dotimes (i n)
      (push (nth i rope) rev-seq))
    (dotimes (i n)
      (setf (nth i rope) (nth i rev-seq)))))

(defun knot-hash (input &key (rounds 64))
  (let* ((in (prep-hash-input input))
         (rope (circularize (iota 256)))
         (rope-start rope)
         (skip 0))
    (dotimes (r rounds)
      (dolist (n in)
        (reverse-first-n-segment n rope)     ; reverse first n elements
        (setf rope (nthcdr (+ n skip) rope)) ; advance rope n places
        (incf skip)))                        ; increase skip
    (mapcar #'condense (result-blocks rope-start))))

;; Output
(defun result-blocks (rope)
  "Return the 256 element rope as 16 blocks of 16"
  (flet ((first-16 ()
           (loop for i from 0 to 15
                 collect (nth i rope))))
    (loop repeat 16
          collect (first-16)
          do (setf rope (nthcdr 16 rope))))) ; advance to the next

(defun condense (sparse-byte)
  (reduce #'logxor sparse-byte))

(defun base16-string (list-of-numbers)
  (format nil "~{~(~2,'0x~)~}" list-of-numbers))


;;; Verification
(defparameter *day-10-puzzle-input* "157,222,1,2,177,254,0,228,159,140,249,187,255,51,76,30")
(defparameter *day-10-answer* "2b0c9cc0449507a0db3babd57ad9e8d8")

(defun knot-hash-checks-p ()
  (if (equal (base16-string (knot-hash *puzzle-input-2*))
             *day-10-answer*)
      T))

;;;; Day 14 (new stuff)

;;; Solution, Part 1: 8194

(defparameter *puzzle-input* "uugsqrei")

(defun count-1-bits (numbers)
  "Returns the count of all on bits in the list of numbers"
  (reduce #'+ numbers :key #'logcount))

(defun bit-count-for-key (&optional (key *puzzle-input*))
  (flet ((hash-input-for-row (n)
           (concatenate 'string key "-" (princ-to-string n))))
    (loop for row from 0 to 127
          summing (count-1-bits
                   (knot-hash
                     (hash-input-for-row row))))))

;;; Solution, Part 2: 1141

(defun nthbit (n number)
  (if (logbitp n number) 1 0))

(defun byte->bits (b)
  (loop for n from 7 downto 0
        collect (nthbit n b)))

(defun bytes->bits (bytes)
  (reduce #'append bytes :key #'byte->bits))

(defun grid-bits-for-key (&optional (key *puzzle-input*))
  (flet ((hash-input-for-row (n)
           (concatenate 'string key "-" (princ-to-string n))))
    (loop for row from 0 to 127
          collect (bytes->bits
                   (knot-hash
                    (hash-input-for-row row))))))

(defun col-pad (row)
  (append '(0) row '(0)))

(defun test-bit-grid (&optional (size 8))
  (loop repeat size
        collect (loop repeat size
                      collect (random 2))))

(defun bit-map-for-key (&optional (key *puzzle-input*))
  (let* ((row-padding (list (make-list 130 :initial-element 0)))
         (col-padded-grid-bits (mapcar #'col-pad (grid-bits-for-key key)))
         (col-and-row-padded-grid-bits (append row-padding
                                               col-padded-grid-bits
                                               row-padding)))
    (make-array '(130 130)
                :element-type 'bit
                :initial-contents col-and-row-padded-grid-bits)))

;; change start to start-row
;; or find a raw to start y, but still loop back to 0?
(defun scan-map-for-1 (map &key (start '(1 1)))
  (let ((max-x (1- (array-dimension map 0)))
        (max-y (1- (array-dimension map 1)))
        (start-x (first start))
        (start-y (second start)))
    (loop named outer-loop
          for x from start-x to max-x do
            (loop for y from 0 to max-y do
              (when (eql 1 (aref map x y))
                (return-from outer-loop (list x y))))
          finally (return-from outer-loop nil))))

(defun neighbor-addresses (locus)
  (let ((x (first locus))
        (y (second locus)))
    (list (list (1+ x) y)
          (list (1- x) y)
          (list x (1+ y))
          (list x (1- y)))))

;; TD - map needs to be passed along too. hmm....
(defun erase-region (map start)
  (let ((x (first start))
        (y (second start)))
    (when (eql 1 (aref map x y))
      (setf (aref map x y) 0)
      (mapc #'erase-region (neighbor-addresses start)))))

(defun region-count (map)
  (labels ((erase-region (start)
             (let ((x (first start))
                   (y (second start)))
               (when (eql 1 (aref map x y))
                 (setf (aref map x y) 0)
                 (mapc #'erase-region (neighbor-addresses start))))))
    (loop for start = '(1 1) then (scan-map-for-1 map)
          while start
          counting (erase-region start))))
