;;;; Day 21, Fractal Art

;; TD: maybe try out iterate in this project?
;; TD: try out defpackage??
(ql:quickload "cl-ppcre")
(ql:quickload "alexandria")
(ql:quickload "arrow-macros")
(use-package :arrow-macros)

;;; input: "input_day21.txt"

;;; Nomenclature:
;;; A 'grid' is the full image at the start and end of each iteration. 
;;; A 'tile' is a square sub-part, either 2x2 or 3x3 for breaking a grid down...
;;; ...or 3x3 or 4x4 result of an expansion rule, which are assembeled back into the new grid.


;; Helper macros
(defmacro make-tile (size &rest key-args)
  `(make-array (list ,size ,size) :element-type 'bit ,@key-args))

(defmacro size-of (tile)
  `(array-dimension ,tile 0))

(defun copy-tile (tile)
  (let ((copied (make-tile (size-of tile))))
    (loop for i below (array-total-size tile)
          do (setf (row-major-aref copied i)
                   (row-major-aref tile i)))
    copied))

;;; Prelude
(defun lines-from-file (file-name)
  (with-open-file (in file-name)
    (loop for line = (read-line in nil)
          while line
          collect line)))

(defun bit-list-from-string (string)
  "Does what it says on the tin."
  (map 'list
       (lambda (c) (- (char-code c) 48))
       string))

(defun make-tile-with-contents (contents)
  "Returns a tile with given contents, specifed as a list of lists."
  (make-tile (length contents) :initial-contents contents))

(defun rule-for-line (line)
  "Return a list of two tiles representing the target and expansion of a rule."
  (-<>> line
        (cl-ppcre:regex-replace-all "\\." <> "0") ; .'s are 0s
        (cl-ppcre:regex-replace-all "\\#" <> "1") ; #'s are 1s
        (cl-ppcre:split "\\ => ")                 ; split into target and expansion
        (mapcar (lambda (string)
                  (->> string
                       (cl-ppcre:split "\\/")             ; split into rows
                       (mapcar #'bit-list-from-string)))) ; rows into bit lists
        (mapcar #'make-tile-with-contents)))

(defun rules-from-file (file-name)
  "Returns a hash-table of expansion rules: tile (a 2D array) -> expansion (lager 2D array)."
  (let ((rules-hash (make-hash-table :test #'equalp))
        (rules-list (mapcar #'rule-for-line (lines-from-file file-name))))
    (loop for (target expansion) in rules-list do
      (loop for permutation in (permutations target) do
        (setf (gethash permutation rules-hash) expansion)))
    rules-hash))

;;; Grids and Tiles
(defun tile-from-grid (tile-size grid row-offset col-offset)
  "Return a size-by-size array from grid (a 2D-array), starting at offset (a 2D-index)."
  (let ((tile (make-tile tile-size)))
    (loop for i below tile-size do
      (loop for j below tile-size do
        (setf (aref tile i j)
              (aref grid (+ i row-offset) (+ j col-offset)))))
    tile))

(defun tiles-from-grid (grid)
  "Returns list of component tiles (size 2 for an even sized grid, size 3 otherwise."
  (let* ((grid-size (array-dimension grid 0))
         (tile-size (if (evenp grid-size) 2 3))
         (tiles-per-side (/ grid-size tile-size)))
    (loop for i below tiles-per-side append
      (loop for j below tiles-per-side
            collect (tile-from-grid tile-size grid (* i tile-size) (* j tile-size))))))

(defun read-tile-into-grid (tile grid row-offset col-offset)
  "Read a tile (3x3 or 4x4 array) into grid (a 2D-array), starting at offset (a 2D-index)."
  (destructuring-bind (rows cols) (array-dimensions tile)
    (loop for i below rows do
      (loop for j below cols do
        (setf (aref grid (+ i row-offset) (+ j col-offset))
              (aref tile i j)))))
  grid)

(defun grid-from-tiles (tiles)
  "Returns grid assembeled from tiles, taken in row-major order."
  (let* ((tiles-per-side (sqrt (length tiles)))
         (tile-size (size-of (first tiles)))
         (grid-size (* tile-size
                       tiles-per-side))
         (grid (make-tile grid-size)))
    (loop for i below tiles-per-side do
      (loop for j below tiles-per-side do
        (read-tile-into-grid (pop tiles) grid (* i tile-size) (* j tile-size))))
    grid))

;;; Permutating
(defmacro rotate-arefs (array &rest refs)
  "Each ref must be a list of one or more indices."
  `(rotatef ,@(loop for ref in refs
                    collect `(aref ,array ,@ref))))

(defun rotate (tile)
  (let* ((rotated (copy-tile tile))
         (size (size-of tile))
         (max (1- size)))
    (rotate-arefs rotated (0 0) (0 max) (max max) (max 0))
    (when (= size 3)
      (rotate-arefs rotated (0 1) (1 2) (2 1) (1 0)))
    rotated))

(defun rotations (tile)
  "Return tile and it's other 3 orientations."
  (loop repeat 4
        for r = tile then (rotate r)
        collect r))

(defun flip (tile)
  "Return horizontally flipped copy of tile."
  (let ((flipped (copy-tile tile))
        (size (size-of tile)))
    (loop for row below size do
      (rotatef (aref flipped row 0)
               (aref flipped row (1- size))))
    flipped))

(defun permutations (tile)
  "Returns 8 permutations: the orginal, a flip, and 3 rotations of each of those."
  (append (rotations tile)
          (rotations (flip tile))))

;;; Solving

(defun expand-grid (grid rules)
  (->> (tiles-from-grid grid)
    (mapcar (lambda (target) (gethash target rules)))
    (grid-from-tiles)))


(defun expand-n-times (n start-grid rules)
  (loop for grid = start-grid then (expand-grid grid rules)
        repeat n
        finally (return grid)))

(defun count-in-tile (bit tile)
  (loop for i below (array-total-size tile)
        count (= bit
                 (row-major-aref tile i))))

;; Solution, Part 1: 179
;; Solution, Part 2: 2,766,750
(defun solve (&optional (iterations 5))
  (let ((rules (rules-from-file "input_day21.txt"))
        (start (make-tile-with-contents '((0 1 0)(0 0 1)(1 1 1)))))
    (count-in-tile 1 (expand-n-times iterations start rules))))

;;; Dev/Debug
(defun print-matrix (matrix)
  (destructuring-bind (rows cols) (array-dimensions matrix)
    (loop for r below rows do
      (loop for c below cols do
            (princ (aref matrix r c)))
      (princ #\Newline)))
  (values))

(defun rule-format (matrix)
  (destructuring-bind (rows cols) (array-dimensions matrix)
    (loop for r below rows do
      (loop for c below cols do
        (princ (aref matrix r c)))
      (princ #\/))
    (princ #\Newline))
  (values))
