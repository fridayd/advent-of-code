;;;; --- Day 11: Hex Ed ---

(defparameter *puzzle-input* "Day 10, Input.txt")

(defun read-input-file (&optional (filename *puzzle-input*))
  (with-open-file (in filename)
    (read-line in nil)))

(ql:quickload "cl-ppcre")

(defun preprocess-input (&optional (line (read-input-file)))
  (let* ((step1 (cl-ppcre:regex-replace-all "," line " "))
         (step2 (cl-ppcre:regex-replace ".*" step1 "(\\&)")))
    (read-from-string step2)))

(defparameter *location* #(0 0 0))

(defun distance-from-origin ()
  (reduce #'max *location* :key #'abs))

(defun move (direction)
  (symbol-macrolet ((x (elt *location* 0))
                    (y (elt *location* 1))
                    (z (elt *location* 2)))
    (case direction
      (n (incf y) (decf z))
      (s (decf y) (incf z))
      (nw (decf x) (incf y))
      (se (incf x) (decf y))
      (ne (incf x) (decf z))
      (sw (decf x) (incf z)))))

(defun run-input ()
  (let ((moves (preprocess-input)))
    (mapc #'move moves)))

;;; Solution, Part 1: 784
(defun solve-part1 ()
  (run-input)
  (distance-from-origin))

;;; Solution, Part 2: 1558
(defun distances-during-run ()
  (let ((moves (preprocess-input)))
    (mapcar #'(lambda (m)
                (move m)
                (distance-from-origin))
            moves)))

(defun solve-part2 ()
  (reduce #'max (distances-during-run)))
