;;;; Day 3, Spiral Memory

(defparameter *puzzle-input* 368078)

;;; Part 1
;;; Solution: 371

;; A spiral address is a number of steps taken along spiral path,...
;; ...starting with 1 at the center/origin of the spiral.
;; A run is a series of steps all moving in the same direction—right, up, left, or down.
(defun manhattan-distance-for-spiral-address (given-address)
  (loop named spiral-walk
    for run-length = 1 then (1+ run-length) ; In a spiral, the run length increases...
    with current-address = 1
    with current-distance = 0
    do
       (loop repeat 2                         ; ...every two runs.
         do
            (loop for i from 1 to run-length
                  do
                     (if (<= i (floor run-length 2))
                         (decf current-distance)     ; the first half of a run decreases the distance, ...
                         (incf current-distance))    ; ... while the second half increases it.
                     (incf current-address)
                     (if (>= current-address given-address)
                         (return-from spiral-walk current-distance))))))

;;; Part 2
;;; Solution: 369601

;; Unit vectors for the eight directions (or Moore neighborhood members).
(defparameter *ordinals* '((-1 -1) (-1 1) (1 -1) (1 1))) ; aka "corners" '(#(-1 -1) #(-1 1) #(1 -1) #(1 1))
(defparameter *cardinals* '((1 0) (0 1) (-1 0) (0 -1)))  ; aka right, up, left, and down
(defparameter *neighbors* (append *ordinals* *cardinals*))

(defun circular-copy (item-list)
  (let ((fresh-copy (copy-list item-list)))
    (nconc fresh-copy fresh-copy)))

(defun move-from (locus direction)
  (mapcar #'+ locus direction))

(defun values-for-keys (table keys)
  (mapcar #'(lambda (k) (gethash k table))
          keys))

(defun neighbors (locus)
  (mapcar #'(lambda (n) (mapcar #'+ locus n))
       *neighbors*))

(defun neighborhood-sum (grid locus)
  (let ((neighborhood (neighbors locus)))
    (apply #'+
           (remove NIL
                   (values-for-keys grid
                                    neighborhood)))))

(defun find-stress-test-value (target-value)
  (loop named spiral-walk
        for run-length = 1 then (1+ run-length)
        with directions = (circular-copy *cardinals*)
        with current-locus = '(0 0)
        with grid = (make-hash-table :test 'equal)
        initially
           (setf (gethash current-locus grid) 1)
        do
           (loop repeat 2
                 do
                    (loop repeat run-length
                          with direction = (pop directions)
                          do
                             (setf current-locus
                                   (move-from current-locus direction))
                             (let ((current-value (neighborhood-sum grid current-locus)))
                               (if (> current-value
                                      target-value)
                                   (return-from spiral-walk current-value)
                                   (setf (gethash current-locus grid)
                                         current-value)))))))
